#!/usr/bin/env node
var express = require("express");
var db = require('./db');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var path = require("path");
const port = 3000;

var allClients = [];
/*var users1 = {
    p_id: "b501c922-874f-49b4-800a-03cc5043ff39",
    user_id: "1",
    user_name: "dhara"
};*/
var users = {
    p_id: "",
    user_id: "",
    user_name: ""
};
var onlineUsers = [];

io.on('connection', function (socket) 
{
    console.log('user connected...');
    // console.log(onlineUsers);
   //socket.send({ value: users1, type: "peer_connection" });
    //when server gets a message from a connected user 
    socket.on('message', function (message) 
    {
        //accepting only JSON messages 
        try {
            data = JSON.parse(message);
        } catch (e) {
            console.log("Invalid JSON");
            data = {};
        }
        //switching type of the user message 
        switch (data.type) 
        {

            case "get_onlineusers":
                console.log("get_onlineusers called");
                console.log(users);
                socket.send({ "onlineUser": users, type: "list" });
                break;

            case "login":
                console.log("login called");
                console.log(data);
                if (data.value != null) {
                    var details = data.value;
                    if (!details.device_id || !details.nickname || !details.registration_token
                        || !details.gender) {
                        console.log("Please provide all required field");
                        return false;
                    }
                    if (details.email && !details.mobile) 
                    {

                        db.query('SELECT * FROM user WHERE email = ' + details.email + ';', function (err, result) {
                            if (err) throw err;
                            if (result.length > 0) {
                                var c_id = result[0].id;
                                var c_username = result[0].nickname;
                            }
                            if (result.length == 0) {
                                try {
                                    db.query('INSERT INTO user SET ?',
                                        {
                                            'device_id': details.device_id,
                                            'nickname': details.nickname,
                                            'email': details.email,
                                         // 'mobile': details.mobile,
                                            "gender": details.gender,
                                            "registration_token": details.registration_token
                                        }
                                        , function (err, res) {
                                            if (err) {
                                                throw err;
                                            }
                                            if (res) {
                                                console.log("data added successfully through email..");
                                                // socket.send({ type: "response", value: result.insertId });
                                                console.log({ type: "response", value: { user_id: res.insertId, user_name: details.nickname } });
                                                socket.send({ type: "response", value: { user_id: res.insertId, user_name: details.nickname } });
                                            }
                                        }
                                    );
                                }
                                catch (e) {
                                    console.log(e);
                                }
                            }
                            else {
                                console.log("Same email found..");
                                var x = 'UPDATE user' + ' ' +
                                    'SET' + ' ' +
                                    'device_id = "' + details.device_id + '", ' +
                                    'registration_token = "' + details.registration_token + '", ' +
                                    'nickname = "' + details.nickname + '" ' +
                                    'WHERE' + ' ' +
                                    'id = ' + c_id + ' ' +
                                    'LIMIT 1' +
                                    ';';
                                console.log(x);
                                db.query(
                                    'UPDATE user' + ' ' +
                                    'SET' + ' ' +
                                    'device_id = "' + details.device_id + '", ' +
                                    'registration_token = "' + details.registration_token + '", ' +
                                    'nickname = "' + details.nickname + '" ' +
                                    'WHERE' + ' ' +
                                    'id = ' + result.id + ' ' +
                                    'LIMIT 1' +
                                    ';', function (err, res) {
                                        if (err) {
                                            throw err;
                                        }
                                        if (res) {
                                            console.log({ type: "response", value: { user_id: c_id, user_name: details.nickname } });
                                            // socket.send({ type: "response", value: result.id });
                                            socket.send({ type: "response", value: { user_id: c_id, user_name: details.nickname } });
                                        }
                                    }
                                );
                            }
                        });

                    }

                    if (details.mobile && !details.email) {

                        db.query('SELECT * FROM user WHERE mobile = ' + details.mobile + ';', function (err, result) {
                            if (err) throw err;
                            if (result.length > 0) {
                                var c_id = result[0].id;
                                var c_username = result[0].nickname;
                            }
                            if (result.length == 0) {
                                try {
                                    db.query('INSERT INTO user SET ?',
                                        {
                                            'device_id': details.device_id,
                                            'nickname': details.nickname,
                                            // 'email': details.email,
                                            'mobile': details.mobile,
                                            "gender": details.gender,
                                            "registration_token": details.registration_token
                                        }
                                        , function (err, res) {
                                            if (err) {
                                                throw err;
                                            }
                                            if (res) {
                                                console.log("data added successfully through mobile..");
                                                // socket.send({ type: "response", value: result.insertId });
                                                console.log({ type: "response", value: { user_id: res.insertId, user_name: details.nickname } });
                                                socket.send({ type: "response", value: { user_id: res.insertId, user_name: details.nickname } });
                                            }
                                        }
                                    );
                                }
                                catch (e) {
                                    console.log(e);
                                }
                            }
                            else {
                                console.log("Same mobile found..");
                                var x = 'UPDATE user' + ' ' +
                                    'SET' + ' ' +
                                    'device_id = "' + details.device_id + '", ' +
                                    'registration_token = "' + details.registration_token + '", ' +
                                    'nickname = "' + details.nickname + '" ' +
                                    'WHERE' + ' ' +
                                    'id = ' + c_id +
                                    ';';
                                console.log(x);
                                db.query(
                                    'UPDATE user' + ' ' +
                                    'SET' + ' ' +
                                    'device_id = "' + details.device_id + '", ' +
                                    'registration_token = "' + details.registration_token + '", ' +
                                    'nickname = "' + details.nickname + '" ' +
                                    'WHERE' + ' ' +
                                    'id = ' + c_id +
                                    ';', function (err, res) {
                                        if (err) {
                                            throw err;
                                        }
                                        if (res) {
                                            // console.log(res);
                                            // socket.send({ type: "response", value: result.id });
                                            console.log({ type: "response", value: { user_id: c_id, user_name: details.nickname } });
                                            socket.send({ type: "response", value: { user_id: c_id, user_name: details.nickname } });
                                        }
                                    }
                                );
                            }
                        });

                    }
                }
                break;

            case "peer_connection":
                console.log("peer_connection called");
                if (data.value != null) 
                {
                    user_id = data.value.user_id;

                    //io.to(socket.id);
                    //console.log(socket.id);
                    if(0 < onlineUsers.length)
                    {
                        for(var i = 0; i < onlineUsers.length; i++) 
                        {
                            if(onlineUsers[i].user_id != user_id) 
                            {
                                onlineUsers.push(data.value);
                                

                                io.to(socket.id).send({ "onlineUser": onlineUsers, type: "list" });
                                console.log(onlineUsers);
                                break;
                               
                            }
                            else
                            {
                                
                                io.to(socket.id).send({ "onlineUser": onlineUsers, type: "list" });
                                console.log(onlineUsers);
                                break;
                            }    
                        }
                    }
                    else
                    {
                        onlineUsers.push(data.value);
                        
                        io.to(socket.id).send({ "onlineUser": onlineUsers, type: "list" });
                        console.log(onlineUsers);
                    }    
                    

                    
                }
                break;

            case "call":
                console.log("call called");
                if (data.value != null) 
                {
                    callerid = data.value.caller_id;
                    receiverid = data.value.receiver_id;
                    onlineUsers.splice(onlineUsers.indexOf(onlineUsers.find(onlineUsers => onlineUsers.user_id == callerid)), 1);
                    onlineUsers.splice(onlineUsers.indexOf(onlineUsers.find(onlineUsers => onlineUsers.user_id == receiverid)), 1);
                   
                    console.log(onlineUsers);

                    socket.send({ "onlineUser": onlineUsers, type: "list" });
                    
                }
                break;

            case "disconnect":
                console.log("disconnect called");
                if (data.value != null) 
                {
                    console.log(data);
                    
                    userid = data.value.user_id;
                    onlineUsers.splice(onlineUsers.indexOf(onlineUsers.find(onlineUsers => onlineUsers.user_id == userid)), 1);
                   
                    //console.log(onlineUsers);

                    socket.send({ "onlineUser": onlineUsers, type: "list" });
                    // onlineUsers = Object.keys(users);
                    // onlineUsers = users;
                    console.log(users);
                }
                break;

            case "end_call":
                console.log("end_call called");
                if (data.value != null) 
                {
                    var details = data.value;
                    if (!details.caller_id
                        || !details.receiver_id || !details.start_time
                        || !details.end_time || !details.duration) 
                    {
                        console.log("Please provide all required field");
                        return false;
                    }
                    try {
                        db.query('INSERT INTO room SET ?',
                            {
                                'caller_id': details.caller_id,
                                'receiver_id': details.receiver_id,
                                'start_time': details.start_time,
                                'end_time': details.end_time,
                                'duration': details.duration,
                                'date': new Date()
                            }
                            , function (err, res) {
                                if (err) {
                                    throw err;
                                }
                                if (res) {
                                    console.log("data added successfully..");
                                }
                            }
                        );
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
                break;

            case "back":
                console.log("back called");
                if (data.value != null) {
                    console.log(data.value);
                    if (users.p_id == data.value.p_id || users.user_id == data.value.user_id || users.user_name == data.value.user_name) {                        
                        users.p_id = "";
                        users.user_id = "";
                        users.user_name = "";
                    }
                    // onlineUsers = Object.keys(users);
                    // onlineUsers = users;
                    console.log(users);
                }
                break;

            default:
                console.log("default called");
                sendTo(socket, {
                    type: "error",
                    message: "Command not found: " + data.type
                });
                break;
        }
    });

    // socket.send("Hello world");

    socket.on('disconnect', function (data) 
    {
        console.log('Got disconnect!');
        console.log(data);
        delete socket;
        
        console.log(users);
        socket.send({ "onlineUser": users, type: "list" });
    });
});

function sendTo(socket, message) {
    socket.send(JSON.stringify(message));
}

app.use(express.static(__dirname + '/node_modules'));

//app.get('/', function (req, res) {
     //res.sendFile(path.join(__dirname + '/index.html'));
//     //__dirname : It will resolve to your project folder.
 //});

server.listen(port, function () {
    console.log("server is listning on port: " + port);
});
